# Simple Rest Library for Unity

**Installation:** Download the SimpleREST.unitypackage file from the master branch and import into your Unity project

**Quick start:** Visit the Wiki for a quick start guide.

**Documentation:** Clone the project and open HtmlDocumentation/index.html in your browser.

This is a simple cure for all your UnityWebRequest and JSON pains - focus on the game, not HTTP and JSON.

### With Simple REST, you can do this:

```
MockAPI.users.Get<User>(queryParameters: "1")
    .Then(response =>
    {
        User user = response.body;
        long statusCode = response.statusCode;
        
        // Do stuff
    })
    .Catch(error =>
    {
        Debug.Log($"Whoops, an error happened: {error.Message}");
    });
```

Yes, you saw that right, User is a data transfer object class, seamlessly deserialized:
```
[Serializable]
public class User
{
    public int id;
    public string createdAt;
    public string name;
    public string avatar;

    public override string ToString()
    {
        return $"{id}, {createdAt}, {name}, {avatar}";
    }
}
```

### "But what about lists, that's annoying, right?" Nope!

```
MockAPI.users.GetList<User>()
    .Then(response =>
    {
        List<User> userList = response.body;
        foreach (var userDto in userList)
        {
            Debug.Log(userDto);
        }
    });
```

### "Alright, alright... Surely it can't handle DICTIONARY requests and responses, right?" Wrong again!

```
MockAPI.users.Get<Dictionary<string, string>>()
    .Then(response =>
    {
        // response.body is a dictionary -> string key-value pairs
    });
```

### "This only works for GET requests, though?" This is really getting silly. Just import the library, you'll love it.

### Features:

**1. Built-in support for authorization tokens**
- When once set, the authorization token is automatically injected into the header of each request

**2. Uses promises to handle asynchronous operations**
- Supports "Then()" if the operation succeeded
- Supports "Catch()" if the operation failed
- Supports chaining promises (.then.then.then...)

**3. Makes handling JSONs easy**
- Built in support for any System.Serializable class
- Built in support for Lists
- Built in support for Dictionaries
- Uses the powerful Json.NET library

**4. Makes talking to multiple APIs as easy as pie**
- Just write a client class for each API

**5. Type less, do more**
- Saves you from boilerplate code
- Very concise and accessible
